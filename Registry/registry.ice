#
# ---------------------------------------------
#  begin ice model variables and configuration
# ---------------------------------------------
#
# declare icing package and choose which icing scheme (Currently only 1)
#
#<key>   <name>     <namelist choice>     <unused>    <associated 4d scalars>
#
package  ice_blade   iceblade_opt==1           -           ice_accre:acc_cyl,acc_bld1,acc_bld2,acc_bld3;ice_ablat:abl_cyl,abl_bld1,abl_bld2,abl_bld3;state:bld_colfac,bld_frzfac,cyl_colfac,cyl_frzfac,mvd,bld_ws,tmp

# outputs to atm model
#<Table> <Type> <Sym>       <Dims>     <Use>   <TLev> <Stagger> <IO>     <DNAME>   <DESCRIP>         <UNITS>
# Accumulation
state    real   -             ikjf       ice_accre  1         -       -    - 
state    real   acc_cyl       ikjf       ice_accre  1         -      rh   "ACCRE_CYL"  "Ice Accretion on standard cylinder" "kg"
state    real   acc_bld1      ikjf       ice_accre  1         -      rh   "ACCRE_BLD1" "Ice Accretion on Vestas  V90-1.8MW turbine" "kg"
state    real   acc_bld2      ikjf       ice_accre  1         -      rh   "ACCRE_BLD2" "Ice Accretion on Vestas  V90-3.0MW turbine" "kg"
state    real   acc_bld3      ikjf       ice_accre  1         -      rh   "ACCRE_BLD3" "Ice Accretion on Vestas V112-3.0MW turbine" "kg"

# Ablation
state    real   -             ikjf       ice_ablat  1         -       -    -
state    real   abl_cyl       ikjf       ice_ablat  1         -      rh   "ABLAT_CYL"  "Ice Ablation on standard cylinder" "kg"
state    real   abl_bld1      ikjf       ice_ablat  1         -      rh   "ABLAT_BLD1" "Ice Ablation on Vestas  V90-1.8MW turbine" "kg"
state    real   abl_bld2      ikjf       ice_ablat  1         -      rh   "ABLAT_BLD2" "Ice Ablation on Vestas  V90-3.0MW turbine" "kg"
state    real   abl_bld3      ikjf       ice_ablat  1         -      rh   "ABLAT_BLD3" "Ice Ablation on Vestas V112-3.0MW turbine" "kg"

# diagnostics only
state    real     bld_colfac   ikj     ice_blade  1         -     -    "BLDCOLFAC"  "Collision efficiency on turbine blade" "ratio"
state    real     bld_frzfac   ikj     ice_blade  1         -     -    "BLDFRZFAC"  "Freezing efficiency on turbine blade" "ratio"
state    real     mvd          ikj     ice_blade  1         -     -    "MVD"        "Median Volumetric Diameter" "m"
state    real     bld_ws       ikj     ice_blade  1         -     -    "BLDWS"      "Wind speed on turbine blade" "m/s"
state    real     tkts         ikj     ice_blade  1         -     -    "TKTS"       "Time with temperatures above 0 C" "s"

# iceblade configure namelist variables
#
#<Table>  <Type>  <Sym>         <How set>          <Nentries>   <Default>
rconfig   integer iceblade_opt  namelist,ice_blade   max_domains      0     rh  "ice_opt"      "Turbine Icing flag for domain"          ""
rconfig   integer accum_opt     namelist,ice_blade   max_domains      1     rh  "accum_opt"    "Accumulation option: 1-Makkonen"
rconfig   integer ablat_opt     namelist,ice_blade   max_domains      1     rh  "ablat_opt"    "Ablation option: 1-DTU"
rconfig   integer num_turbs     namelist,ice_blade       1            1     rh  "num_turbs"    "Number of turbines"
rconfig   real    bld_diam      namelist,ice_blade   max_domains     1.     rh  "bld_diam"     "Effective diameter of turbine blade" "m"
rconfig   logical adj_bld_ws    namelist,ice_blade       1         .false.  rh  "adj_ws"       "Adjust wind speed to turbine relative?"

#
# -------------------------------------------
#  end ice model variables and configuration
# -------------------------------------------
